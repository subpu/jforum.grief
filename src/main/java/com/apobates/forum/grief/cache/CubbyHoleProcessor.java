package com.apobates.forum.grief.cache;

import java.util.Collection;
import java.util.Set;

/**
 * CubbyHole调用的处理器
 * @param <T>
 * @author xiaofanku
 * @since 20200726
 */
@FunctionalInterface
public interface CubbyHoleProcessor<T> {
    /**
     * 处理程序
     * 
     * @param action 动作记录
     * @return 执行成功的对像执行CubbyHole::toChecksum结果的集合
     */
    Set<String> process(Collection<T> action);
}