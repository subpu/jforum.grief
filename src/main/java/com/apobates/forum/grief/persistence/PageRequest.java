package com.apobates.forum.grief.persistence;

/**
 * 分页查询参数类型的一个实现
 *
 * @author xiaofanku
 * @since 20200510
 */
public final class PageRequest implements Pageable {
    private final static long serialVersionUID = 1L;
    private final int page;
    private final int pageViewRecords;

    public PageRequest(int page, int pageSize) {
        this.page = page;
        this.pageViewRecords = pageSize;
    }

    @Override
    public int getOffset() {
        return (page - 1) * pageViewRecords;
    }

    @Override
    public int getPageNumber() {
        return page;
    }

    @Override
    public int getPageSize() {
        return pageViewRecords;
    }
}
