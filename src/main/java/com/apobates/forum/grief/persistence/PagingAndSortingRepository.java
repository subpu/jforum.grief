package com.apobates.forum.grief.persistence;

import java.io.Serializable;
import java.util.stream.Stream;

/**
 * 分页查询DAO接口
 *
 * @author xiaofanku
 * @param <T>
 * @param <ID>
 * @since 20200510
 */
public interface PagingAndSortingRepository<T, ID extends Serializable> extends DataRepository<T, ID> {
    /**
     * 分页查询所有记录
     *
     * @param pageable
     * @return 空结果集返回一个空列表
     */
    Page<T> findAll(Pageable pageable);

    /**
     * 无数据时的结果模板 当总记录数为0时直接调用这个方法
     *
     * @return
     */
    default Page<T> emptyResult() {
        return new Page<T>() {
            @Override
            public long getTotalElements() {
                return 0L;
            }

            @Override
            public Stream<T> getResult() {
                return Stream.empty();
            }
        };
    }
}
