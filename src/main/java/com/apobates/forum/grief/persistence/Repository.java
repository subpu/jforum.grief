package com.apobates.forum.grief.persistence;

import java.io.Serializable;

/**
 * 标记接口
 * @author xiaofanku
 * @param <T> 实例类型
 * @param <ID> 主键ID类型
 * @since 20200510
 */
public interface Repository<T, ID extends Serializable> {
}
