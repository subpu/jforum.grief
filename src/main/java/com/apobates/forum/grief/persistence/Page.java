package com.apobates.forum.grief.persistence;

import com.apobates.forum.grief.lang.EntityPageResult;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 分页查询返回类型
 *
 * @author xiaofanku
 * @param <T>
 * @since 20200510
 */
public interface Page<T> {
    /**
     * 总记录数
     *
     * @return
     */
    long getTotalElements();

    /**
     * 结果集
     *
     * @return
     */
    Stream<T> getResult();

    /**
     * 空的分页
     * @param <T>
     * @return
     */
    public static <T> Page<T> empty(){
        return new Page<>(){
            @Override
            public long getTotalElements() {
                return 0L;
            }

            @Override
            public Stream<T> getResult() {
                return Stream.empty();
            }
        };
    }

    /**
     * 生成分页结果值对象
     * @param pageable
     * @return
     */
    default EntityPageResult<T> toResult(Pageable pageable){
        return new EntityPageResult<>(
                getTotalElements(),
                pageable.getPageNumber(),
                pageable.getPageSize(),
                getResult().collect(Collectors.toList()));
    }
}
