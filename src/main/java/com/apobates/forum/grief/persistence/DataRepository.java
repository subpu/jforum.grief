package com.apobates.forum.grief.persistence;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * 通用的Dao接口
 *
 * @author xiaofanku
 * @param <T>
 * @param <ID>
 * @since 20200510
 */
public interface DataRepository<T, ID extends Serializable> extends Repository<T, ID> {
    /**
     * 保存实体
     *
     * @param entity 实体
     */
    void save(T entity);

    /**
     * 查看实体
     *
     * @param primaryKey 实体主键
     * @return 可能返回null
     */
    Optional<T> findOne(ID primaryKey);

    /**
     * 编辑实体
     *
     * @param updateEntity 实体
     * @return 成功返回Optional.of(true)
     */
    Optional<Boolean> edit(T updateEntity);

    /**
     * 实体所有记录
     *
     * @return 无结果集返回一个空列表
     */
    Stream<T> findAll();

    /**
     * 实体的总数量
     *
     * @return 空结果返回0
     */
    long count();
}
