package com.apobates.forum.grief.lang.mapper;

import com.apobates.forum.grief.lang.ResponseResult;
import com.google.gson.Gson;

/**
 * 响应结果的WEB映射函数
 * @param <T>
 */
public class HtmlResponseResultMapper<T> implements ResponseResultMapper<T, String> {
    private final String type;

    /**
     * json
     */
    private final static String JSON = "application/json";
    /**
     * XML
     */
    //private final static String XML = "application/atom+xml";
    /**
     * json格式结果转换器
     */
    public final static HtmlResponseResultMapper JSON_TYPE = new HtmlResponseResultMapper<>(JSON);
    /**
     * xml格式结果转换器
     */
    //private final static HtmlResponseResultMapper XML_TYPE = new HtmlResponseResultMapper<>(XML);

    private HtmlResponseResultMapper(String mediaType) {
        this.type = mediaType;
    }

    @Override
    public String map(ResponseResult<T> result) {
        if(type.equalsIgnoreCase(JSON)) {
            return toJson(result);
        }
        /*
        if(type.equalsIgnoreCase(XML)){
            return toXml(result);
        }*/
        return null;
    }

    private String toJson(Object result){
        return new Gson().toJson(result);
    }

    public static void main(String[] args) {
        String res = HtmlResponseResultMapper.JSON_TYPE.map(ResponseResult.notFound());
        System.out.println(res);
    }
}
