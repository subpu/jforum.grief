package com.apobates.forum.grief.lang.mapper;

import com.apobates.forum.grief.lang.ResponseResult;

/**
 * 响应结果映射函数
 * @param <T>
 * @param <U>
 */
@FunctionalInterface
public interface ResponseResultMapper<T,U> {
    /**
     * 响应结果转换
     * @param result
     * @return
     */
    U map(ResponseResult<T> result);
}
