package com.apobates.forum.grief.lang;

import com.apobates.forum.grief.lang.mapper.HtmlResponseResultMapper;
import com.apobates.forum.grief.lang.mapper.ResponseResultMapper;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 分页结果值对象
 * @param <T>
 */
public class EntityPageResult<T> implements Serializable {
    /**
     * 总数量
     */
    private long total;
    /**
     * 页码值
     */
    private int page;
    /**
     * 每页显示的数量
     */
    private int size;
    /**
     * 结果集
     */
    private List<T> result;

    public EntityPageResult() {
    }

    /**
     *
     * @param total 总数量
     * @param page 当前页码值
     * @param size 每页显示的数量
     * @param result 结果集
     */
    public EntityPageResult(long total, int page, int size, List<T> result) {
        this.total = total;
        this.page = page;
        this.size = size;
        this.result = result;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int pageSize) {
        this.size = pageSize;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public static <T> EntityPageResult<T> empty(int pageSize){
        EntityPageResult<T> rs = new EntityPageResult<>();
        rs.setTotal(0L);
        rs.setPage(1);
        rs.setSize(pageSize);
        rs.setResult(Collections.emptyList());
        return rs;
    }

    public <U> U map(ResponseResultMapper<EntityPageResult<T>,U> mapper){
        ResponseResult<EntityPageResult<T>> data = new ResponseResult<>(200, "ok", this);
        return mapper.map(data);
    }

    public static void main(String[] args) {
        List<Student> rs  = Arrays.asList(
                new Student(10, "xiaofanku"),
                new Student(12, "zhansan"));
        EntityPageResult<Student> result = new EntityPageResult<>(2, 1, 10, rs);
        HtmlResponseResultMapper<EntityPageResult<Student>> mapper = HtmlResponseResultMapper.JSON_TYPE;
        String map = result.map(mapper);
        System.out.println(map);
    }

    static class Student implements Serializable{
        private int age;
        private String names;

        public Student(int age, String names) {
            this.age = age;
            this.names = names;
        }

        public int getAge() {
            return age;
        }

        public String getNames() {
            return names;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void setNames(String names) {
            this.names = names;
        }
    }
}
