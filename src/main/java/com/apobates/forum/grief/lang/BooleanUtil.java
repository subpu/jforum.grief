package com.apobates.forum.grief.lang;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;
/**
 * 布尔工具类
 *
 * @author xiaofanku
 * @since 20221002
 */
public final class BooleanUtil {
    /**
     * 逻辑与
     */
    public final static BinaryOperator<Boolean> ANDOpFUN = Boolean::logicalAnd;
    /**
     * 逻辑或
     */
    public final static BinaryOperator<Boolean> OROpFUN = Boolean::logicalOr;
    /**
     * 布尔数组中是否全是true
     * @param values 布尔数组
     * @return
     */
    public static Boolean isTrue(Boolean ... values){
        return Stream.of(values).allMatch(n -> n == true);
    }

    /**
     * 布尔数组中是否全是true
     * @param values 布尔数组
     * @return
     */
    public static boolean isPrimitiveTrue(boolean ... values){
        return BooleanUtils.and(values);
    }

    /**
     * 将Boolean数组包装成集合
     * @param boolArray 布尔数组
     * @return
     */
    public static Collection<Boolean> boxBool(boolean ... boolArray){
        return Arrays.asList(ArrayUtils.toObject(boolArray));
    }

    /**
     * 将Boolean集合拆成数组
     * @param boolSet 布尔集合
     * @return
     */
    public static boolean[] unBoxBool(Collection<Boolean> boolSet){
        Boolean[] data = new Boolean[boolSet.size()];
        return ArrayUtils.toPrimitive(boolSet.toArray(data));
    }

    /**
     * 使用指定的操作函数来决断布尔集合的值
     * @param boolSet 布尔集合
     * @param binOp 操作函数
     * @return
     */
    public static Boolean decide(Collection<Boolean> boolSet, BinaryOperator<Boolean> binOp){
        return boolSet.stream().reduce(Boolean.FALSE, binOp);
    }

    private BooleanUtil(){}
}
