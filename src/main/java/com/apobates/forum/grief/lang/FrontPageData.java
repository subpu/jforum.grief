package com.apobates.forum.grief.lang;

/**
 * 前端分页
 * @author xiaofanku
 * @since 20200526
 */
public interface FrontPageData {
    /**
     * 分页的连接地址
     * @return
     */
    String getPageURL();
    /**
     * 每页显示的记录数
     * @return
     */
    int getPageSize();
    /**
     * 总记录数
     * @return
     */
    long getRecords();
    /**
     * 分页的页码
     * @return
     */
    int getPage();
}
