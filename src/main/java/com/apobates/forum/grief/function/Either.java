package com.apobates.forum.grief.function;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class Either<E> {
    private final Predicate<E> condition;
    private final E operand;
    private Boolean result;

    private Either(E operand, Predicate<E> condition) {
        this.condition = condition;
        this.operand = operand;
    }

    public static <E> Either<E> of(E element, Predicate<E> condition) {
        return new Either<>(element, condition);
    }

    public Either<E> thenIf(Consumer<E> consumer) {
        if (null == result){
            result = condition.test(operand);
        }
        if (result) {
            consumer.accept(operand);
        }
        return this;
    }

    public Either<E> orElse(Consumer<E> consumer) {
        if (null == result) {
            result = condition.test(operand);
        }
        if (!result) {
            consumer.accept(operand);
        }
        return this;
    }

    public E get() {
        return operand;
    }

    public static void main(String[] args) {
        Integer data=10;
        Predicate<Integer> c = (d)->null !=d && d.compareTo(100) == 1;
        Consumer<Integer> tc = (d1)-> System.out.println("T:"+d1);
        Consumer<Integer> fc = (d2)-> System.out.println("F:"+d2);
        Either.of(data, c)
                .thenIf(tc)
                .orElse(fc)
                .get();
    }
}
