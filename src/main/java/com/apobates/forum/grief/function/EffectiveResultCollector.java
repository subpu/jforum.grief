package com.apobates.forum.grief.function;

/**
 * EffectiveResult的合并器
 * @param <T>
 */
@FunctionalInterface
public interface EffectiveResultCollector<T> {
    /**
     * 映射/转换
     * @param oldDate 旧值
     * @param newDate 新值
     * @return 返回映射后的结果
     */
    T map(T oldDate, T newDate);
}