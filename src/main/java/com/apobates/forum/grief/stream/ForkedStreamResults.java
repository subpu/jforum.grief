package com.apobates.forum.grief.stream;

public interface ForkedStreamResults {

    /**
     * Get the completable future encapsulating the result of a fork
     *
     * @param key the memento associated with the fork
     * @param <R> the type of results of the fork
     * @return the completable future encapsulating the result of the fork
     */
    <R> R get(Object key);
}
